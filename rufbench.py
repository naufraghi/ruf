#!/usr/bin/env python
from __future__ import unicode_literals, print_function

import sys
import subprocess

def bench_ls():
    subprocess.check_output(["ls", "-aR", "testdir/5levels10files"])
    
def bench_ruf():
    subprocess.check_output(["target/release/ruf", "testdir/5levels10files"])
    
    

if __name__ == "__main__":
    print("Building release ruf")
    subprocess.check_call(["cargo", "build", "--release"])
    print("benchmark ls -aR")
    subprocess.check_call([sys.executable, "-m", "timeit", "-s", "import rufbench", "rufbench.bench_ls()"])
    print("benchmark ruf")
    subprocess.check_call([sys.executable, "-m", "timeit", "-s", "import rufbench", "rufbench.bench_ruf()"])
    
