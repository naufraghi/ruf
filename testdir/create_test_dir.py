#!/bin/env python
#-*- encoding: utf-8 -*-

import os

def create_medium_dir(nfiles, nsubdirs):
    root = os.path.join(os.path.dirname(__file__), "%dlevels%dfiles" % (nsubdirs, nfiles))
    if not os.path.exists(root):
        os.mkdir(root)
    def create_subdirs(basedir, nfiles, nsubdirs, depth=0):
        for i in xrange(nfiles):
            filename = os.path.join(basedir, "{0:02d}.txt".format(i))
            open(filename, 'w+')
        for i in xrange(nsubdirs - depth):
            dirname = os.path.join(basedir, "dir-{0:02d}".format(i))
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            create_subdirs(dirname, nfiles, nsubdirs, depth=depth+1)
    create_subdirs(root, nfiles, nsubdirs)
    
if __name__ == "__main__":
    create_medium_dir(10, 5)
    