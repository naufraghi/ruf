# README #

Learning [Rust](http://rust-lang.org) by reimplementing [the silver searcher (ag)](http://geoff.greer.fm/ag/) or [the platinum searcher (pt)](https://github.com/monochromegane/the_platinum_searcher), a Go clone of the first one.

Using `pt` as a example, the code is clean and compact, I'm implementing the `find` functionality of this high level view:

```
func (p *PlatinumSearcher) Search() error {
	pattern, err := p.pattern()
	if err != nil {
		return err
	}
	grep := make(chan *GrepParams, p.Option.Proc)
	match := make(chan *PrintParams, p.Option.Proc)
	done := make(chan struct{})
	go p.find(grep, pattern)
	go p.grep(grep, match)
	go p.print(match, done)
	<-done
	return nil
}
```
(exerpt from [search.go](https://github.com/monochromegane/the_platinum_searcher/blob/v1.7.6/search.go#L9))

`find` means:

- recursive directory listing
- .[git|hg]ignore parsing
- user pattern matching

