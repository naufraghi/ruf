extern crate ruf;

use std::env;
use std::fs::DirEntry;
use std::ffi::OsString;
use std::path::PathBuf;
use std::collections::BinaryHeap;

fn main() {
    let fuzzy_file_pattern = env::args_os().nth(1).map(|fuzz| fuzz.into_string().unwrap_or("".into()));
    let fuzz_dist = ruf::rufdist(fuzzy_file_pattern);

    let rootdir = match env::args_os().nth(2) {
        Some(userdir) => userdir,
        None => OsString::from("."), // use current dir
    };

    let mut candidates = BinaryHeap::new();

    {
    let patterns =  ruf::DirPatterns::new(PathBuf::from(&rootdir));
    let filter = &mut |entry: DirEntry| {
                    let path = entry.path().into_os_string().into_string().unwrap_or("".into());
                    let (a, b) = fuzz_dist(&path);
                    candidates.push((a+b, path));
                    if candidates.len() > 10 {
                        // pop the worst candidate
                        candidates.pop();
                    }
                 };
    ruf::visit_dirs_patterns(patterns, filter).expect("Errore!");
    }
    for &(_, ref path) in candidates.into_sorted_vec().iter().rev() {
        println!("{}", path);
    }
}
