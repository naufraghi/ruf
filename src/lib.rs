#![feature(test)]

extern crate test;
extern crate glob;

use std::io;
use std::cmp;
use std::rc::Rc;
use std::fs::{self, DirEntry};
use std::path::PathBuf;
use std::string::ToString;

use glob::Pattern;


/*
 * `visit_dirs` is taken from the `read_dir` documentation, in a simple test
 * a recursive print is ~5 slower then `ls -r`, mostly because of the function
 * stack overhead.
 */

pub fn visit_dirs(dir: PathBuf, cb: &mut FnMut(DirEntry)) -> io::Result<()> {
    if dir.is_dir() {
        for entry in try!(fs::read_dir(dir)) {
            let entry = try!(entry);
            if entry.path().is_dir() {
                try!(visit_dirs(entry.path(), cb));
            } else {
                cb(entry);
            }
        }
    }
    Ok(())
}

/* In this version we avoid the recursion overhead using a 'folder-to-explore' stack
*/

pub fn visit_dirs_stack(dir: PathBuf, cb: &mut FnMut(DirEntry)) -> io::Result<()> {
    if dir.is_dir() {
        let mut folders = vec![dir];
        loop {
            let path = match folders.pop() {
                None => break,
                Some(p) => p,
            };
            for entry in try!(fs::read_dir(path)) {
                let entry = try!(entry);
                let subpath = entry.path();
                if subpath.is_dir() {
                    folders.push(subpath);
                } else {
                    cb(entry);
                }
            }
        }
    }
    Ok(())
}

#[derive(Debug)]
pub struct DirPatterns {
    parent: Option<Rc<DirPatterns>>,
    path: PathBuf,
    patterns: Option<Vec<Pattern>>,
}

impl DirPatterns {
    pub fn new(path: PathBuf) -> DirPatterns {
        DirPatterns {
            parent: None,
            path: path,
            patterns: None,
        }
    }
}

pub fn descend(parent: &Rc<DirPatterns>, other: PathBuf, patterns: Option<Vec<Pattern>>) -> Rc<DirPatterns> {
    let dp = DirPatterns {
        parent: Some(parent.clone()),
        path: other,
        patterns: patterns,
    };
    Rc::new(dp)
}

pub fn visit_dirs_patterns(dir_patterns: DirPatterns, cb: &mut FnMut(DirEntry)) -> io::Result<()> {
    if dir_patterns.path.is_dir() {
        let mut folders = vec![Rc::new(dir_patterns)];
        let skip = |sp: &PathBuf| {
            sp.ends_with(".git/") | sp.ends_with(".hg/") | sp.ends_with(".svn/")
        };
        loop {
            match folders.pop() {
                Some(parent) => {
                    for entry in try!(fs::read_dir(&parent.path)) {
                        let entry = try!(entry);
                        let subpath = entry.path();
                        if subpath.is_dir() {
                            if !skip(&subpath) {
                                let child = descend(&parent, subpath, None);
                                folders.push(child);
                            }
                        } else {
                            cb(entry);
                        }
                    }
                }
                None => break,
            }
        }
    }
    Ok(())
}

pub fn levenshtein_distance<T, R>(s1: &T, s2: &R) -> usize
where T: ToString, R: ToString {
    let v1: Vec<char> = s1.to_string().chars().collect();
    let v2: Vec<char> = s2.to_string().chars().collect();
    let v1len = v1.len();
    let v2len = v2.len();

    // Early exit if one of the strings is empty
    if v1len == 0 { return v2len; }
    if v2len == 0 { return v1len; }

    let min3 = |v1, v2, v3| cmp::min(v1, cmp::min(v2, v3));
    let delta = |x, y| if x == y { 0 } else { 1 };

    let mut column: Vec<usize> = (0..v1len+1).collect();
    for x in 1..v2len+1 {
        column[0] = x;
        let mut lastdiag = x-1;
        for y in 1..v1len+1 {
            let olddiag = column[y];
            column[y] = min3(column[y] + 1,
                             column[y-1] + 1,
                             lastdiag + delta(v1[y-1], v2[x-1]));
            lastdiag = olddiag;
        }
    }
    column[v1len]
}


pub fn lvdist<T: 'static, R>(opatt: Option<T>) -> Box<Fn(&R) -> usize>
where T: ToString, R: ToString {
    if opatt.is_some() {
        let patt = opatt.unwrap();
        Box::new(move |targ| levenshtein_distance(&patt, targ))
    } else {
        Box::new(move |_| 0)
    }
}

pub fn ruf_distance<T, R>(pattern: &T, path: &R) -> (usize, usize)
where T: ToString, R: ToString {
    let v1: Vec<char> = pattern.to_string().chars().collect();
    let v2: Vec<char> = path.to_string().chars().collect();
    // We specialize the common case where the target path is
    // longer than the pattern.
    // In that case we consider as distance the minimum skipping
    // prefixes form path:
    // 1) pattern /this/is/a/full.path
    // 2) pattern  this/is/a/full.path
    // 3) pattern   his/is/a/full.path
    // 4) ...
    // ...
    // n) pattern              ll.path
    //                         pattern
    fn delta(x: char, y: char) -> usize {
        match (x, y) {
            (a, b) if a == b => 0,
            ('e', '3') | ('3', 'e') => 2,
            ('a', '4') | ('4', 'a') => 2,
            ('l', '1') | ('1', 'l') => 2,
            ('t', '7') | ('7', 't') => 2,
            (_, '/') => 2,  // a non expressed / is less important
            ('/', _) => 20, // than a missed one
            _ => 10,
        }
    }

    // use lambda to avoid fn signature
    let min3 = |v1, v2, v3| cmp::min(v1, cmp::min(v2, v3));

    let mut distances: Vec<(usize, usize)> = vec![];
    let s1 = &v1[..]; // pattern
    let s1len = s1.len();
    for (i, s2) in v2.windows(s1len*2).enumerate() {
        let s2len = s2.len();

        // Early exit if one of the strings is empty
        if s1len == 0 { return (s2len, s2len); }
        if s2len == 0 { return (s1len, s2len); }

        let mut column: Vec<usize> = (0..s1len+1).collect();
        for x in 1..s2len+1 {
            column[0] = x;
            let mut lastdiag = x-1;
            for y in 1..s1len+1 {
                let olddiag = column[y];
                column[y] = min3(column[y] + 10,
                                 column[y-1] + 10,
                                 lastdiag + delta(s1[y-1], s2[x-1]));
                lastdiag = olddiag;
            }
        }
        let res = (column[s1len], i);
        distances.push(res);
    }
    *distances.iter().min().unwrap_or(&(2*s1len, 2*s1len))
}

pub fn rufdist<T: 'static, R>(opatt: Option<T>) -> Box<Fn(&R) -> (usize, usize)>
where T: ToString, R: ToString {
    match opatt {
        Some(patt) => Box::new(move |targ| ruf_distance(&patt, targ)),
        None => Box::new(move |_| (0, 0)),
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    use std::rc::Rc;
    use std::path::PathBuf;
    use glob::Pattern;

    #[test]
    fn test_visit() {
        visit_dirs(PathBuf::from("."),
                   &mut |entry| {
                        println!("{}", entry.path().into_os_string().into_string().unwrap())
                   }).unwrap();
    }

    #[test]
    fn test_visit_stack() {
        visit_dirs_stack(PathBuf::from("."),
                         &mut |entry| {
                             println!("{}", entry.path().into_os_string().into_string().unwrap())
                         }).unwrap();
    }

    #[test]
    fn test_visit_patterns() {
        let dir_patterns = DirPatterns {parent: None,
                                        path: PathBuf::from("."),
                                        patterns: None};
        visit_dirs_patterns(dir_patterns,
                            &mut |entry| {
                               println!("{}", entry.path().into_os_string().into_string().unwrap())
                            }).unwrap();
    }

    #[test]
    fn test_ignores1() {
        let _1 = DirPatterns {parent: None,
                              path: PathBuf::from("."),
                              patterns: Some(vec![Pattern::new("*").unwrap()])};
        let _2 = DirPatterns {parent: None,
                              path: PathBuf::from("."),
                              patterns: None};
    }

    #[test]
    fn test_ignores2() {
        let i1 = DirPatterns {parent: None,
                              path: PathBuf::from("a"),
                              patterns: Some(vec![Pattern::new("1*").unwrap()])};
        let _2 = DirPatterns {parent: Some(Rc::new(i1)),
                              path: PathBuf::from("a/b"),
                              patterns: Some(vec![Pattern::new("2*").unwrap()])};
    }

    #[test]
    fn test_ignores2debug() {
        let i1 = Rc::new(DirPatterns {parent: None,
                                      path: PathBuf::from("a"),
                                      patterns: Some(vec![Pattern::new("1*").unwrap()])});
        let i2 = DirPatterns {parent: Some(i1.clone()),
                              path: PathBuf::from("a/b"),
                              patterns: Some(vec![Pattern::new("2*").unwrap()])};
        println!("i1 = {:?}", &i1);
        println!("12 = {:?}", &i2);
    }

    #[test]
    fn test_descend_patterns() {
        let i1 = Rc::new(DirPatterns {parent: None,
                                      path: PathBuf::from("a"),
                                      patterns: Some(vec![Pattern::new("1*").unwrap()])});
        let i2 = descend(&i1,
                         PathBuf::from("a/b"),
                         Some(vec![Pattern::new("2*").unwrap()]));
        println!("i1 = {:?}", &i1);
        println!("12 = {:?}", &i2);
    }

    #[test]
    fn test_levenshtein_distance() {
        let s1 = "Hello world!";
        let s2 = "H€llo wrld!";
        let d = levenshtein_distance(&s1, &s2);
        assert!(d == 2);
    }

    #[test]
    fn test_levenshtein_distance2() {
        let s1 = "Hello world!";
        let s2 = String::from("Hellø wrld!");
        let d = levenshtein_distance(&s1, &s2);
        assert!(d == 2);
    }

    #[test]
    fn test_ruf_distance1() {
        let s = "demo/py";
        let f1 = "src/demo/main.py";
        let f2 = "src/demo_main.py";
        let d1 = ruf_distance(&s, &f1);
        let d2 = ruf_distance(&s, &f2);
        assert!(d1 < d2, "{:?} < {:?} not verified", d2, d1);
    }

    #[test]
    fn test_ruf_distance2() {
        let s = "demopy";
        let f1 = "src/demo/main.py";
        let f2 = "src/foobar/demo.py";
        let d1 = ruf_distance(&s, &f1);
        let d2 = ruf_distance(&s, &f2);
        assert!(d2 < d1, "{:?} < {:?} not verified", d2, d1);
    }

    #[test]
    fn test_lvdist() {
        let pat = Some("some");
        let lv = lvdist(pat);
        let s = "dome";
        assert!(lv(&s) == 1);
    }

    #[test]
    fn test_lvdist_none() {
        let none: Option<String> = None;
        let lv = lvdist(none);
        let s = "dome";
        assert!(lv(&s) == 0);
    }

    #[bench]
    fn bench_test_visit(b: &mut Bencher) {
        b.iter(|| visit_dirs(PathBuf::from("testdir/5levels10files"), &mut |_entry| {}).unwrap());
    }
    #[bench]
    fn bench_test_visit_hot(b: &mut Bencher) {
        b.iter(|| visit_dirs(PathBuf::from("testdir/5levels10files"), &mut |_entry| {}).unwrap());
    }
    #[bench]
    fn bench_test_visit_stack(b: &mut Bencher) {
        b.iter(|| visit_dirs_stack(PathBuf::from("testdir/5levels10files"), &mut |_entry| {}).unwrap());
    }
    #[bench]
    fn bench_test_visit_patterns(b: &mut Bencher) {
        b.iter(|| {
            let dir_patterns = DirPatterns {parent: None,
                                            path: PathBuf::from("testdir/5levels10files"),
                                            patterns: None};
            visit_dirs_patterns(dir_patterns, &mut |_entry| {}).unwrap()}
            );
    }
}
